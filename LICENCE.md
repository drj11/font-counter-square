# This is not the licence

This is not a licence that allows you to use the font.

This is the text that _will become_ the licence,
once the font and the licence are packaged for an authorised
font distribution site.


## Text of non-licence


# Cubic Type

Thank you choosing and downloading this Cubic Type font.
I hope it brings you peace and joy.


## Installation

The font files themselves are supplied as `.otf` files.
Generally double-clicking or right-clicking and
selecting _Install_ should work.

Some applications may require that you restart them or
restart your computer before the freshly installed font is
recognised.


## Grant

The fonts and the font files are the intellectual property of
Cubic Type.
You are permitted to make use of the fonts under the terms of
this licence.

If you are using the font for Welsh language or
Icelandic language then any use is permitted.

Otherwise you are permitted to use the font
(including commercial use) as long as that use does not involve
distributing the font.

Generally permitted:

- posters
- cards
- (printed) book
- rendering into video
- logo
- clothing
- product labels and packaging


Generally not allowed:

- press printing
- apps
- servers

In particular:
you are not permitted to embed the font in a PDF
and distribute it;
you are not permitted to distribute the font to your print shop;
you are not permitted to distribute the font to your
friends, coworkers, employees, clients, suppliers;
you are not permitted to install the font on a
server (internal or web) in a way that distributes the font to users;
you are not permitted to embed the font in
an application (web or mobile or console or similar);
you are not permitted to embed the font in an e-book.


## Additional grants

All fonts are available for private licensing and
many are available for off-the-shelf commercial licensing.
Those licences will allow you to do thngs that this licence does not.


## About me

My name is David Jones, i am first Typographer at Cubic Type and
i've been creating fonts since 2020.

Check out [my portfolio and contact
details](https://about.cubictype.com/).

# END
