# Spacing

Following [TRICKS].

I already did a rough spacing for the letters in _artzoid_.


Set n to 70 (no change).
Happy with that.
left-edge left-leg to left-edge right-leg distance
is about equal to left-edge right-leg
to left-edge left-leg (of following n) distance.
More exactly than is reasonable.

- Set o to 60 (no change).
- Set n to 60 n 70 (to balance with o better).
- Set b to 60.
- Set d as n (no change, already done).
- Set g h k m u y as n.
- Set i to 60 i 10 (no change, already done).
- Set j to 0 j 10 (no change, already done). Will need kerning.
- Set l to 70 (no change).
- Set p q s þ to 60 (as b).
- Set r to 60 r 40.
- Set c e to 60 c 50.
- Set v w to 30 v 30.
- Set x to 40 x 40.
- Set a to 70 a 60 (reflection of n; no change).
- Set f to 60 f 40.
- Set t to 50 t 60
- Set z to 70 z 50 (no change).
- Set ð to 60 ð 30 (note horizontal stroke).

# END
